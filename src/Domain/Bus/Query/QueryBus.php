<?php

declare(strict_types=1);

namespace DoctorI\Shared\CommandQueryBus\Domain\Bus\Query;

interface QueryBus
{
    public function ask(Query $query): ?Response;
}
