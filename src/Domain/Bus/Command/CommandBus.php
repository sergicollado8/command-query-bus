<?php

declare(strict_types=1);

namespace DoctorI\Shared\CommandQueryBus\Domain\Bus\Command;

interface CommandBus
{
    public function dispatch(Command $command): void;
}
