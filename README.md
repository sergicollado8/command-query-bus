# Command Query Bus

![source](https://img.shields.io/badge/source-doctori/command--query--bus-blue)
![version](https://img.shields.io/badge/version-1.0.0-blue.svg)
![php](https://img.shields.io/badge/php->=8.0-8892bf)

Command Query Bus models to use in Doctor I applications.

## 🚀 Environment Setup

### 🛠️ Install dependencies
Install the dependencies if you haven't done it previously: `make build` or `composer install`

### ✅ Tests execution
Execute PHPUnit tests:
```bash
$ make run-test
```

### 🎨 Style
This project follows PSR-12 style
```bash
$ make check-style

$ make fix-style
```

### 🧐 Inspection
This project should be inspected by PHPStan
```bash
$ make inspect-phpstan
```

## 🔥 Usage

### 🛠️ Install in a third application
Include the package with composer.
```bash
$ composer require doctori/command-query-bus
```

Add the following lines on your service.yaml with the tag that you use

```
imports:
    - { resource: ../vendor/doctori/command-query-bus/config/services.yaml }

DoctorI\Shared\CommandQueryBus\Infrastructure\Bus\Command\InMemorySymfonyCommandBus:
  arguments: [ !tagged doctori.command_handler ]

DoctorI\Shared\CommandQueryBus\Infrastructure\Bus\Query\InMemorySymfonyQueryBus:
  arguments: [ !tagged doctori.query_handler ]
```
