current-dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

.PHONY: build
build: deps

.PHONY: deps
deps: composer-install

# 🐘 Composer
composer-env-file:
	@if [ ! -f .env.local ]; then echo '' > .env.local; fi

.PHONY: composer-install
composer-install: CMD=install

.PHONY: composer-update
composer-update: CMD=update

.PHONY: composer-require
composer-require: CMD=require
composer-require: INTERACTIVE=-ti --interactive

.PHONY: composer-require-module
composer-require-module: CMD=require $(module)
composer-require-module: INTERACTIVE=-ti --interactive

.PHONY: composer
composer composer-install composer-update composer-require composer-require-module: composer-env-file
	@docker run --rm $(INTERACTIVE) --volume $(current-dir):/app --user $(id -u):$(id -g)  \
    		composer:2 $(CMD) \
    			--ignore-platform-reqs \
    			--no-ansi

.PHONY: run-tests
run-tests:
	@docker-compose run doctori-command-query-bus-php vendor/bin/phpunit

.PHONY: rebuild
rebuild:
	make deps

.PHONY: check-style
check-style:
	@docker-compose run doctori-command-query-bus-php vendor/bin/phpcs --standard=PSR12 src tests --runtime-set ignore_errors_on_exit 1 --runtime-set ignore_warnings_on_exit 1

.PHONY: fix-style
fix-style:
	@docker-compose run doctori-command-query-bus-php vendor/bin/phpcbf --standard=PSR12 src tests

.PHONY: inspect-phpstan
inspect-phpstan:
	@docker-compose run doctori-command-query-bus-php vendor/bin/phpstan analyse -c phpstan.neon
